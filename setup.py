#!/usr/bin/env python

"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path
import os

here = path.abspath(path.dirname(__file__))

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='UPSAL',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='0.1.0',
    description='Using Python and Spark form Anomaly detection',

    # Author details
    author='UPSAL',


    maintainer='Thomas Prikkel, Thomas Helling and Lars Hopman',

    packages=find_packages(exclude=['contrib', 'docs', 'tests*', 'deploy']),

    entry_points={
        'console_scripts': ['upsalctl=UPSAL.UPSAL:main'],
    },

    setup_requires=[],
    tests_require=[],

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=requirements,

)
