import findspark

findspark.init()

import pyspark
from pyspark import SparkContext, SparkConf
from pyspark.sql.session import SparkSession
from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark.sql.functions import *
from pyspark.sql.types import *


class SparkInitializer:
    def __init__(self, config):
        self.config = config
        try:
            self.conn = SparkConf().setAppName(config.get("project", "app_name")).set("spark.executor.memory",
                                                                                      config.get("spark",
                                                                                                 "spark.executor.memory")).setMaster(
                config.get("spark", "master"))
        except:
            raise

        # Create a local StreamingContext with two working thread and batch interval of 1 second
        self.sc = SparkContext(conf=self.conn)
        self.sc.setLogLevel("ERROR")
        self.spark = SparkSession(self.sc)

    def load_log_data(self, data_file):
        return self.sc.textFile(data_file, 16)

    def convert_to_dataframe(self, lines):
        rows = lines.map(lambda l: Row(l))
        return rows.toDF(["line"])

    def create_columns_for_dataframe(self, df, delimiter, with_columns=None, udf_columns=None, drop_columns=None):
        split_col = pyspark.sql.functions.split(df["line"], delimiter)

        if with_columns is not None:
            for withColumn in with_columns:
                if len(withColumn) > 1:
                    df = df.withColumn(withColumn[0], eval(withColumn[1]))
                else:
                    exec(withColumn[0])

        if udf_columns is not None:
            for udf_column in udf_columns:
                tmp_udf = udf(eval(udf_column['lambda']), eval(udf_column['type']))
                df = df.withColumn(udf_column['df_column_name'], tmp_udf(eval(udf_column['lambda_input'])))

        if drop_columns is not None:
            for drop_column in drop_columns:
                df = df.drop(drop_column)

        return df
