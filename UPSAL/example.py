import os

from helpers.ConfigHelper import get_config
from framework import Framework

import pickle

import time


def main():
    config = get_config(os.getcwd() + '/config/config.ini')
    framework = Framework(config)

    """File loading"""
    with_columns = [
        ['hour', "floor(split_col.getItem(0) / 3600)"],
        ['source', "split_col.getItem(1)"],
        ['destination', "split_col.getItem(2)"],
        ['source_comp', "split_col.getItem(3)"],
        ['destination_comp', "split_col.getItem(4)"],
        ['auth_type', "split_col.getItem(5)"],
        ['logon_type', "split_col.getItem(6)"],
        ['auth_orien_old', "split_col.getItem(7)"],
        ['feedback', "split_col.getItem(8)"],
        ['source', "split_col.getItem(1)"],
        ["split_lin = pyspark.sql.functions.split(df['source'], '@')"],
        ['user', "split_lin.getItem(0)"],
        ['user', "regexp_replace('user', '[^A-Za-z0-9]+', '',)"],
        ['domain', "split_lin.getItem(1)"],
        ["split_lin = pyspark.sql.functions.split(df['destination'], '@')"],
        ['dest_user', "split_lin.getItem(0)"],
        ['dest_user', "regexp_replace('dest_user', '[^A-Za-z0-9]+', '',)"]
    ]

    udf_columns = [
        {
            'lambda': 'lambda x: 1 if x[0] == "LogOn" and x[1] == "Success" else 0',
            'type': 'IntegerType()',
            'lambda_input': "array('auth_orien_old', 'feedback')",
            'df_column_name': 'logon_success'
        },
        {
            'lambda': 'lambda x: 1 if x[0] == "LogOn" and x[1] == "Fail" else 0',
            'type': 'IntegerType()',
            'lambda_input': "array('auth_orien_old', 'feedback')",
            'df_column_name': 'logon_fail'
        },
        {
            'lambda': 'lambda x: 1 if x[0] == "LogOn" else 0',
            'type': 'IntegerType()',
            'lambda_input': "array('auth_orien_old')",
            'df_column_name': 'auth_orien'
        },
        {
            'lambda': 'lambda x: 1 if x[0] == x[1] else 0',
            'type': 'IntegerType()',
            'lambda_input': "array('user', 'dest_user')",
            'df_column_name': 'source_dest'
        },
    ]

    drop_columns = [
        "auth_orien_old",
        "line",
        "feedback"
    ]

    path = '/var/scratch/ddm1701/auth_1.txt'
    delimiter = ','

    df = framework.load_data(path, delimiter, with_columns, udf_columns, drop_columns)
    df.show()

    """Calculating stuff"""
    df = framework.apply_preprocessing(df)
    start_time = time.time()
    feature_dfs = framework.apply_features(df)
    print "--- " + str(time.time() - start_time) + " seconds ---"

    """Detect anomalies based on Feature dfs"""

    print "ANOMALIES LOGIN"
    userFeatures = feature_dfs['userFeatures']
    anomalies_login = userFeatures.where((userFeatures["attempted_logins"] > ( userFeatures["attempted_logins_avg"] + 3 * userFeatures["attempted_logins_stddev"])) &
                                         (userFeatures["perc_succes"] < ( userFeatures["perc_succes_avg"] - 3 * userFeatures["perc_succes_stddev"]))
                                        )

    print "ANOMALIES UNIQUE"
    anomalies_unique = userFeatures.where((userFeatures["unique_dest_users"] > ( userFeatures["unique_dest_users_avg"] + 3 * userFeatures["unique_dest_users_stddev"])) &
                                          (userFeatures["unique_src_computers"] > ( userFeatures["unique_src_computers_avg"] + 3 * userFeatures["unique_src_computers_stddev"])) &
                                          (userFeatures["unique_dest_computers"] > ( userFeatures["unique_dest_computers_avg"] + 3 * userFeatures["unique_dest_computers_stddev"]))
                                         )

    print "ANOMALIES FREQ"
    anomalies_freq = userFeatures.where((userFeatures["perc_same"] > ( userFeatures["perc_same_avg"] + 3 * userFeatures["perc_same_stddev"])) &
                                        (userFeatures["freq_of_source"] > ( userFeatures["freq_of_source_avg"] + 3 * userFeatures["freq_of_source_stddev"])) &
                                        (userFeatures["freq_of_destination"] > ( userFeatures["freq_of_destination_avg"] + 3 * userFeatures["freq_of_destination_stddev"])) &
                                        (userFeatures["freq_of_dest_user"] > ( userFeatures["freq_of_dest_user_avg"] + 3 * userFeatures["freq_of_dest_user_stddev"]))
                                       )


    print "--- " + str(time.time() - start_time) + " seconds ---"

    print 'Number of anomalies freq detected: ' + str(anomalies_freq.count())
    print 'Number of anomalies unique detected: ' + str(anomalies_unique.count())
    print 'Number of anomalies login detected: ' + str(anomalies_login.count())

    datafreem = anomalies_login.toPandas()
    datafreem = datafreem[['user', 'hour']]
    pickle.dump(datafreem, open('anomalies_login.p', 'wb'))

    datafreem2 = anomalies_unique.toPandas()
    datafreem2 = datafreem2[['user', 'hour']]
    pickle.dump(datafreem2, open('anomalies_unique.p', 'wb'))

    datafreem3 = anomalies_freq.toPandas()
    datafreem3 = datafreem3[['user', 'hour']]
    pickle.dump(datafreem3, open('anomalies_freq.p', 'wb'))


    print "--- " + str(time.time() - start_time) + " seconds ---"

    #anomalies.coalesce(1).write.format("com.databricks.spark.csv").option("header", "true").save("output")
    #ramework.send_to_data_store('http://data.anomalies.larshopman.nl', anomalies, "anomaly_7_days", "user")


if __name__ == '__main__':
    main()
