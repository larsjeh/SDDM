import requests
import time
import json


class DataSaver():
    def __init__(self, host,logger):
        self.host = host
        self.logger = logger

    def send(self, experiment_description, df, key, value):

        payload = {
            "name": experiment_description,
            "timestamp": time.time()
        }

        headers = {'Content-Type': 'application/json'}
        r = requests.post(self.host, headers=headers, json=payload)

        if r.status_code == 201:
            data = json.loads(r.text)
            experiment_id = data['id']

            for row in df.rdd.collect():
                # TODO: merge multiple anomalies to reduce requests
                payload = {
                    "username": row[key],
                    "hour": row["hour"],
                    "description": "",  # empty for now
                }

                r = requests.put(self.host + '/experiments/' + experiment_id + '/anomalies', headers=headers, json=payload)

                if r.status_code != 201:
                    self.logger.critical('Error sending ' + row[key] + ' to data store')
