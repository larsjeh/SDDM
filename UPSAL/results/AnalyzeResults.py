from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score


class AnalyzeResults:
    def __init__(self):
        self.results = []
        self.cm = []
        self.predicted = []
        self.actual = []

    def printResults(self):
        """ Prints the results """
        print self.results

    def print_statistics(self, y_true, y_pred, measures=['accuracy', 'precision', 'recall', 'fscore']):
        """ Prints the statistics of the predicted and actual values """

        for measure in measures:
            if measure == 'accuracy':
                print str(accuracy_score(y_true, y_pred))
            # print 'Accuracy score is ' + str(accuracy_score(self.actual, self.predicted))
            elif measure == 'precision':
                print str(precision_score(y_true, y_pred, average='macro'))
            # print 'Precision score is ' + str(precision_score(self.actual, self.predicted, average='macro'))
            elif measure == 'recall':
                print str(recall_score(y_true, y_pred, average='macro'))
            # print 'Recall score is ' + str(recall_score(self.actual, self.predicted, average='macro'))
            elif measure == 'fscore':
                print str(f1_score(y_true, y_pred, average='macro'))
                # print 'F-score is ' + str(f1_score(self.actual, self.predicted, average='macro'))
