import imp,os

class FeaturesManager():
    def __init__(self):
        pass

    def load(self, included_features=[], directory="features"):
        list_features = os.listdir(directory+os.sep+'modules')
        list_features = self.remove_unnecessary_files(list_features)

        feature_classes = []
        for feature_name in list_features:
            if feature_name.split('.')[-1]=='py' and os.path.splitext(feature_name)[0] in included_features:
                feature = imp.load_source('feature', directory+os.sep+'modules'+os.sep+feature_name)
                feature_classes.append(feature.Feature())
        self.feature_classes = feature_classes
        return feature_classes

    def remove_unnecessary_files(self,list_features):
        list_features.remove('__init__.py')

        return list_features

    def get_feature_classes(self):
        return self.feature_classes