from features.feature import FeatureAbstract

import pyspark
import numpy as np

from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
from pyspark.sql.session import SparkSession

import time
from operator import add

from pyspark.sql.types import *
from pyspark.sql.functions import floor
from pyspark.sql import Row
from pyspark.sql.functions import *
from pyspark.sql.window import Window

class Feature(FeatureAbstract):
	def __init__(self):
		self.feature_name = 'userFeatures'

		print "Load feature: " + self.feature_name

	def feature_function(self, df):

		start_time = time.time()

		feature_df = df.groupBy("hour", "user").agg( sum('auth_orien').alias('attempted_logins'), countDistinct("dest_user").alias('unique_dest_users'), countDistinct("source").alias('unique_src_computers'), countDistinct("destination").alias('unique_dest_computers'), (sum('logon_success') / sum('auth_orien')).alias('perc_succes'), (sum('source_dest') / count('source_dest')).alias('perc_same') )

		number_of_sources = df.groupBy("hour", "user", "source").agg(count("source").alias("freq_of_source"))
		freq_src_computer = number_of_sources.groupBy("hour", "user").agg(max("freq_of_source").alias('freq_of_source'))

		feature_df = feature_df.join(freq_src_computer, ['hour', 'user'] )

		number_of_destinations = df.groupBy("hour", "user", "destination").agg(count("destination").alias("freq_of_destination"))
		freq_dest_computer = number_of_destinations.groupBy("hour", "user").agg(max("freq_of_destination").alias('freq_of_destination'))

		feature_df = feature_df.join(freq_dest_computer, ['hour', 'user'] )

		number_of_destination_users = df.groupBy("hour", "user", "dest_user").agg(count("dest_user").alias("freq_of_dest_user"))
		freq_dest_user = number_of_destination_users.groupBy("hour", "user").agg(max("freq_of_dest_user").alias('freq_of_dest_user'))

		feature_df = feature_df.join(freq_dest_user, ['hour', 'user'] )

		w = Window.partitionBy("user").orderBy("hour").rangeBetween(-168, 0)

		columns = feature_df.columns
		columns.remove('hour')
		columns.remove('user')

		for c in columns:
			feature_df = feature_df.withColumn(str(c)+"_avg", avg(feature_df[c]).over(w))
			feature_df = feature_df.withColumn(str(c)+"_stddev", stddev(feature_df[c]).over(w))

		return feature_df
