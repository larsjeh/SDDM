from features.feature import FeatureAbstract

class Feature(FeatureAbstract):
	def __init__(self):
		self.feature_name = 'userEvents'

		print "Load feature: " + self.feature_name

	def feature_function(self):
		user_day_df = self.df.groupBy("day", "userIndex").count()

		return user_day_df