from ConfigParser import SafeConfigParser

def get_config(path):
    config = SafeConfigParser()
    config.read(path)
    return config