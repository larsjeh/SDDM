# Python imports
import os
import json
import os
import json
import logging.config
import time
import json
# Dependencies
import numpy as np
from pyspark.sql.types import *

# Project imports
from classes.SparkInitializer import SparkInitializer
from features.FeaturesManager import FeaturesManager
from preprocessors.PreprocessorManager import PreprocessorManager
from custom_exceptions import DataNotFoundException, PreprocessorsNotFound, FeaturesNotFoundException
import requests
from results.DataSaver import DataSaver


class Framework():
    def __init__(self, config):
        self.config = config
        self._setup_logging()
        self.logger = logging.getLogger(__name__)
        self.SI = self._initialize_spark()

    def _setup_logging(self):
        path = os.getcwd() + 'config/logging.yaml'
        if os.path.exists(path):
            with open(path, 'rt') as f:
                config = json.load(f)
            logging.config.dictConfig(config)
        else:
            logging.basicConfig()

    def _initialize_spark(self):
        self.logger.debug('Initializing Spark')
        return SparkInitializer(self.config)

    def load_raw_file(self, path=None):
        """ Load file from config file if no path is given """
        if path is None:
            self.logger.debug('Trying to load file from ' + self.config.get("project", "data_set"))
            return self.SI.load_log_data(self.config.get("project", "data_set"))

        self.logger.debug('Trying to load file from ' + path)
        return self.SI.load_log_data(path)

    def convert_to_dataframe(self, lines):
        """ Convert the raw file input to a df """
        if lines is None:
            raise DataNotFoundException

        return self.SI.convert_to_dataframe(lines)

    def create_columns_for_dataframe(self, df, delimiter, with_columns=None, udf_columns=None, drop_columns=None):
        """ Create the columns for the df """
        if df is None:
            raise DataNotFoundException

        return self.SI.create_columns_for_dataframe(df, delimiter, with_columns, udf_columns, drop_columns)

    def load_preprocessing_modules(self):
        """ Load the preprocessing manager with the available features """
        self.preprocessor_manager = PreprocessorManager()
        preprocessor_collection = self.preprocessor_manager.load(
            json.loads(self.config.get("project", "preprocessors")))

        if preprocessor_collection is None:
            self.logger.error('No preprocessors found')
            raise PreprocessorsNotFound

        return preprocessor_collection

    def apply_preprocessing(self, df):
        """ Apply the preprocessing defined in the config"""
        self.logger.fatal("STARTING preprocessing")
        self.load_preprocessing_modules()

        for preprocessor in self.preprocessor_manager.get_preprocessor_classes():
            df = preprocessor.preprocessor_function(df)
        self.logger.fatal("FINISHED preprocessing")
        return df

    def load_feature_modules(self):
        """ Load the feature manager with the available features """
        self.features_manager = FeaturesManager()
        features = [f.encode('ascii', 'ignore') for f in json.loads(self.config.get("project", "features"))]
        self.features_manager.load(features)

        if features is None:
            self.logger.error('No features found')
            raise FeaturesNotFoundException

        return features

    def apply_features(self, df):
        """ Apply the features defined in the config"""
        self.logger.fatal("STARTING applying features")

        self.load_feature_modules()

        feature_dfs = {}
        for feature in self.features_manager.get_feature_classes():
            self.logger.fatal("STARTING " + str(feature))
            feature_df = feature.feature_function(df)
            feature_df.show()
            feature_dfs[str(feature)] = feature_df
        self.logger.fatal("FINISHED applying features")
        return feature_dfs

    def load_data(self, path, delimiter, with_columns=None, udf_columns=None, drop_columns=None):
        raw_file = self.load_raw_file(path)
        df = self.convert_to_dataframe(raw_file)
        return self.create_columns_for_dataframe(df, delimiter, with_columns, udf_columns, drop_columns)

    def send_to_data_store(self, host, df, experiment_description, key, value=""):
        DataStore = DataSaver(host ,self.logger)

        DataSaver.send(experiment_description,df,key,value)
        # Send experiment


