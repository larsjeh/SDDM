from preprocessors.preprocessor import PreprocessorAbstract

from pyspark.ml.feature import StringIndexer

class Preprocessor(PreprocessorAbstract):
	def __init__(self):
		self.preprocessor_name = 'userToInteger'
		self.print_name()

	def preprocessor_function(self):
		indexer = StringIndexer(inputCol="user", outputCol="userIndex")
		indexed = indexer.fit(self.df).transform(self.df)
		indexed.show()

		return indexed

