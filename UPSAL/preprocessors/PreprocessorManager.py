import imp, os


class PreprocessorManager():
    def __init__(self):
        pass

    def load(self, included_preprocessors=[], directory="preprocessors"):
        list_preprocessors = os.listdir(directory + os.sep+ 'modules')
        list_preprocessors = self.remove_unnecessary_files(list_preprocessors)

        preprocessor_classes = []
        for preprocessor_name in list_preprocessors:
            if preprocessor_name.split('.')[-1] == 'py' and os.path.splitext(preprocessor_name)[
                0] in included_preprocessors:
                preprocessor = imp.load_source('preprocessor', directory + os.sep + 'modules' + os.sep+ preprocessor_name)
                preprocessor_classes.append(preprocessor.Preprocessor())
        self.preprocessor_classes = preprocessor_classes
        return preprocessor_classes

    def remove_unnecessary_files(self, list_preprocessors):
        list_preprocessors.remove('__init__.py')

        return list_preprocessors

    def get_preprocessor_classes(self):
        return self.preprocessor_classes