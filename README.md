Running the UPSAL framework
=======================

* Make sure Spark is installed on your local machine (http://spark.apache.org/downloads.html).
* Ensure your SPARK_PATH environment variable is set and it is pointing to the correct directory.
* Install the necessary Python dependencies by using `pip install -r requirements.txt`
* Change `config/config.ini` to your needs.